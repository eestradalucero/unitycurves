﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class SelectionInfo {
	
	[MenuItem("Tools/Count Selected Objects", false, 0)]
	public static void ShowInfo(){
		//Selection represents the current selection in the UnityEditor
		Debug.Log( Selection.objects.Length + " Objects selected in the editor" );
	}

	[MenuItem("Tools/Count Selected Objects", true)]	//True means this is a validator
	public static bool ShowInfoValidator(){
		return Selection.objects.Length > 0;
	}

	[MenuItem("Tools/Clear Selection", false, 1)]
	public static void ClearSelection(){
		Selection.activeObject = null;
	}

	[MenuItem("Tools/Clear Selection", true)]
	public static bool ClearSelectionValidation(){
		return Selection.objects.Length > 0;
	}
}
