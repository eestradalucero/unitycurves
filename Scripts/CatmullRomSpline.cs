﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CatmullRomSpline : Spline {
	
	/// <summary>
	/// Gets the point in curve.
	//	http://www.iquilezles.org/www/articles/minispline/minispline.htm
	/// </summary>
	/// <returns>The point in curve.</returns>
	/// <param name="t">T.</param>
	/// <param name="point0">Point0.</param>
	/// <param name="point1">Point1.</param>
	/// <param name="point2">Point2.</param>
	/// <param name="point3">Point3.</param>
	public override Vector3 GetPointInCurve (float t, Vector3 point0, Vector3 point1, Vector3 point2, Vector3 point3){
		Vector3 a = 0.5f * (2f * point1);
		Vector3 b = 0.5f * (point2 - point0);
		Vector3 c = 0.5f * (2f * point0 - 5f * point1 + 4f * point2 - point3);
		Vector3 d = 0.5f * (-point3 + 3f * point1 - 3f * point2 + point3);

		Vector3 p = a + (b * t) + (c * t * t) + (d * t * t * t);

		return p;
	}
		


	



}
