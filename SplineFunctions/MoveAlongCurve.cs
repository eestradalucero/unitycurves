﻿using UnityEngine;
using System.Collections;

public class MoveAlongCurve : MonoBehaviour {

	[SerializeField] CubicBezierSpline path;
	[Range(0f,1f)]
	public float position = 0;

	// Use this for initialization
	void Start () {
		transform.position = path.GetPointInCurveWorldCoordinates(position);
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = path.GetPointInCurveWorldCoordinates(position);
	}
}
