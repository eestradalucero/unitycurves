﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class SplineMesh : MonoBehaviour {

	public CubicBezierSpline path;
	public float lineWidth = 0.1f;
	[Range(1, 50)]
	public int divisionsPerCurve;
	public float uvTiling = 1f;
	private float separationBetweenDivisions { get { return 1f/(float)divisionsPerCurve/(float)	path.curveCount; } }
	public bool isRealTime = false;
	float offset = 1;


	List<Vector3> vertices;
	List<int> triangles;
	Node[,] nodes;
	List<Vector2> uvs;

	void Start(){
		Build();
	}

	[ContextMenu("Build Mesh")]
	void Build(){
		GenerateMesh();
		DisplayMesh();
	}

	public void GenerateMesh(){
		vertices = new List<Vector3>();
		triangles = new List<int>();
		uvs = new List<Vector2>();

		offset = 1 / uvTiling;
		int nodeRowCount = (path.curveCount * divisionsPerCurve) + 1;
		nodes = new Node[nodeRowCount, 2];

		//This creates all the points necesary along the curve.
		for(int i = 0; i < nodeRowCount; i++){
			for(int j = 0; j < 2; j++){
				float newi = separationBetweenDivisions * i;
				Vector3 controlPoint = path.GetPointInCurve(newi);
				Vector3 perpControlPoint = path.PerpendicularOfPoint(newi).normalized * (j == 0 ? 1 : -1);
				//Change Width should be around here
				nodes[i,j] = new Node(controlPoint + perpControlPoint * lineWidth);
			}
		}

		//Creates points 
		Square[] squares = new Square[nodeRowCount-1];
		for(int i = 0; i < squares.Length; i++){
			squares[i] = new Square(nodes[i,	1],		//Top Left
				nodes[i+1,	1],		//Top Right
				nodes[i+1,	0],		//Bottom Right
				nodes[i,	0]);	//Botom Left
		}


		for (int i = 0; i < squares.Length; i++){
			AddVertices(squares[i]);
			AddUVs(squares[i], i);
			AddTriangles(i);
		}
	}

	void AddVertices(Square square){
		vertices.Add(square.topLeft.position);
		vertices.Add(square.topRight.position);
		vertices.Add(square.bottomRight.position);

		vertices.Add(square.bottomRight.position);
		vertices.Add(square.bottomLeft.position);
		vertices.Add(square.topLeft.position);
	}

	void AddUVs(Square square, int index){
		Vector2 uOffsetVect = Vector2.right * offset * index -  Vector2.right * Mathf.Floor(offset * index);

		uvs.Add(uOffsetVect + Vector2.up );
		uvs.Add(uOffsetVect + Vector2.up  + Vector2.right * offset);
		uvs.Add(uOffsetVect + Vector2.right * offset);

		uvs.Add(uOffsetVect + Vector2.right * offset);
		uvs.Add(uOffsetVect + Vector2.zero);
		uvs.Add(uOffsetVect + Vector2.up);
	}

	void AddTriangles(int index){
		triangles.Add( (index * 6) );
		triangles.Add( (index * 6) + 1 );
		triangles.Add( (index * 6) + 2 );
		triangles.Add( (index * 6) + 3 );
		triangles.Add( (index * 6) + 4 );
		triangles.Add( (index * 6) + 5 );
	}



	public void DisplayMesh(){
		Mesh mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;

		mesh.vertices = vertices.ToArray();
		mesh.uv = uvs.ToArray();

		mesh.triangles = triangles.ToArray();
		mesh.RecalculateNormals();
	}

	void Update(){
		if(!isRealTime)
			return;
		Build();
	}

	public class Node {
		public Vector3 position;
		public int vertexIndex = -1;
		
		public Node(Vector3 _pos) {
			position = _pos;
		}
	}

	public class Square {
		
		public Node topLeft, topRight, bottomRight, bottomLeft;
		
		public Square (Node _topLeft, Node _topRight, Node _bottomRight, Node _bottomLeft) {
			topLeft = _topLeft;
			topRight = _topRight;
			bottomRight = _bottomRight;
			bottomLeft = _bottomLeft;
		}
		
	}

}
