﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class CurveMenuItem {
	[MenuItem("Tools/Curves/Create Curve &#c")]
	// # shift, %control, & alt
	public static void CreateCurve(){
		Debug.Log("Create Curve");
		new GameObject("Curve", typeof(CubicBezierSpline));
	}

	[MenuItem("CONTEXT/CubicBezierSpline/Add Spline Mesh")]
	public static void AddSplineMesh(MenuCommand menuCommand){
		CubicBezierSpline spline = (CubicBezierSpline) menuCommand.context;
		spline.gameObject.AddComponent<SplineMesh>();
	}

	[MenuItem("CONTEXT/CubicBezierSpline/Add Spline Mesh", true)]
	public static bool AddSplineMeshValidator(MenuCommand menuCommand){
		CubicBezierSpline spline = (CubicBezierSpline) menuCommand.context;
		var splineMesh = spline.GetComponent<SplineMesh>();
		return splineMesh == null;
	}





}


