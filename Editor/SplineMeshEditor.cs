﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(SplineMesh))]
public class SplineMeshEditor : Editor {

	SplineMesh splineMesh;
	private bool shouldUpdateRealTime = true;

	void OnEnable(){
		splineMesh = (SplineMesh) target;
	}

	public override void OnInspectorGUI () {
		serializedObject.Update();
		DrawDefaultInspector();
		if(GUILayout.Button("Build Curve")){
			splineMesh.GenerateMesh();
			splineMesh.DisplayMesh();
			return;
		}

		shouldUpdateRealTime =  GUILayout.Toggle(shouldUpdateRealTime, "Auto-Update in editor?");
		serializedObject.ApplyModifiedProperties();
	}


	void OnSceneGUI(){
		if(shouldUpdateRealTime){
			splineMesh.GenerateMesh();
			splineMesh.DisplayMesh();
		}
	}
	
}
